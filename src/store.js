import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
// Components
import Reducers from './reducers';

const store = createStore(Reducers, applyMiddleware(thunk));

export default store;