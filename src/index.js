import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
// Components
import store from './store';
import App from './App';

// CSS
import 'bootstrap/dist/css/bootstrap.css';
import '../public/css/index.css';

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);