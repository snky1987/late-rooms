import { combineReducers } from 'redux'
import SearchFormReducers from './App/Search/Reducers'

export default combineReducers({
    SearchFormReducers
});