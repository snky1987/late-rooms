import React, { Component } from 'react';
import { connect } from 'react-redux';
// Components
import Autocomplete from "./autocomplete";
import SearchFilters from "./searchFilters";


class Search extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-3">
                        <SearchFilters />
                    </div>
                    <div className="col-md-9">
                        <Autocomplete name="search"
                                      autoComplete="off"
                                      placeholder="Type your search here..."
                                      label="Search hotels"
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(
    state => state,
    () => {
        return {}
    }
)(Search);