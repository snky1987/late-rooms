import React, { Component } from 'react';
import {connect} from "react-redux";
// Actions
import * as SearchActions from "./Actions";

class Input extends Component {
    render() {
        const {SearchFormReducers} = this.props;
        const {searchInput} = SearchFormReducers;

        return (
            <div>
                <input className={this.props.className}
                       type={this.props.type}
                       name={this.props.name}
                       autoComplete={this.props.autoComplete}
                       placeholder={this.props.placeholder}
                       value={searchInput}
                       onChange={this.props.handleInputChanged}
                />
            </div>
        );
    }
}

export default connect(
    state => state,
    dispatch => {
        return {
            handleInputChanged: (evt) => dispatch(SearchActions.inputChanged(evt.target.value)),
        }
    }
)(Input);
