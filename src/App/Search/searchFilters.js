import React, { Component } from 'react';
import {connect} from "react-redux";
// Components
import SearchFilter from './searchFilter';


class SearchFilters extends Component {
    getStarsRatingOptions() {
        const options = [];
        let rating = '';

        for (let i = 1; i <= 5; i++) {
            rating += '*';

            options.push({
                label: rating,
                value: i
            });
        }

        return options;
    }

    render() {
        return (
            <div>
                <SearchFilter name="starsRating" label="Filter by: Star Rating" options={this.getStarsRatingOptions()} />
            </div>
        );
    }
}


export default connect(
    state => state,
    () => {
        return {}
    }
)(SearchFilters);
