import React, { Component } from 'react';
import {connect} from "react-redux";
import * as SearchActions from "./Actions";

class SearchFilter extends Component {
    onCheckboxClick() {
        const {
            name,
            value
        } = this.props;

        this.props.handleCheckboxClick({
            name,
            value
        });
    }

    render() {
        const {
            label,
            value
        } = this.props;

        return (
            <div>
                <input type="checkbox" name="vehicle" value={value} onClick={this.onCheckboxClick.bind(this)} />
                    {label}
            </div>
        );
    }
}


export default connect(
    state => state,
    dispatch => {
        return {
            handleCheckboxClick: (options) => dispatch(SearchActions.filterChanged(options)),
        }
    }
)(SearchFilter);
