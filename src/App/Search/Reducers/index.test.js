import reducer, {initialState} from './index'
import * as actions from "../Actions";
import {FILTER_OPTION_CLICKED} from "../Actions";

const data = [
    {
        "name":       "hotelone",
        "starRating": 5,
        "facilities": ["car park", "pool"]
    },
    {
        "name":       "hoteltwo",
        "starRating": 3,
        "facilities": ["car park", "gym"]
    },
    {
        "name":       "hotelthree",
        "starRating": 3,
        "facilities": []
    }
];

describe('App/Search/Reducers/index', () => {
    it('should return initial state', () => {
        const action = {
            type: undefined
        };

        expect(reducer(undefined, action)).toEqual(initialState);
    });

    it('should change the input value', () => {
        const expectedState = {
            ...initialState,
            searchInput: 'test'
        };

        const action = {
            type:  actions.INPUT_CHANGED,
            value: 'test'
        };

        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    describe('should filter the list of options by the input value', () => {
        it('and return resultset when the value is found', () => {
            const expectedState = {
                ...initialState,
                searchInput: 'hoteltwo',
                autocompleteOptions: [
                    {
                        "name":       "hoteltwo",
                        "starRating": 3,
                        "facilities": ["car park", "gym"]
                    }
                ]
            };

            const action = {
                type:  actions.INPUT_CHANGED,
                value: 'hoteltwo'
            };

            expect(reducer(initialState, action)).toEqual(expectedState);
        });

        it('and return no results when value is not found', () => {
            const expectedState = {
                ...initialState,
                searchInput: '__hoteltwo',
                autocompleteOptions: []
            };

            const action = {
                type:  actions.INPUT_CHANGED,
                value: '__hoteltwo'
            };

            expect(reducer(initialState, action)).toEqual(expectedState);
        });

        it('and return no results when value is found not case sensitive', () => {
            const expectedState = {
                ...initialState,
                searchInput: 'hotElTwo',
                autocompleteOptions: [
                    {
                        "name":       "hoteltwo",
                        "starRating": 3,
                        "facilities": ["car park", "gym"]
                    }
                ]
            };

            const action = {
                type:  actions.INPUT_CHANGED,
                value: 'hotElTwo'
            };

            expect(reducer(initialState, action)).toEqual(expectedState);
        });
    });

    it('should change the autoselect value', () => {
        const expectedState = {
            ...initialState,
            autocompleteOption: 'test'
        };

        const action = {
            type: actions.AUTOSELECT_OPTION_CLICKED,
            name: 'test'
        };

        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should add filter to filters list if one did not exist', () => {
        const expectedState = {
            ...initialState,
            filters: {
                starsRating: [
                    1
                ]
            },
        };

        const action = {
            type:  FILTER_OPTION_CLICKED,
            name:  'starsRating',
            value: 1
        };

        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should remove filter from filters list if one exists', () => {
        const expectedState = {
            ...initialState,
            filters: {
                starsRating: []
            },
        };

        const action = {
            type:  FILTER_OPTION_CLICKED,
            name:  'starsRating',
            value: 1
        };

        expect(reducer(initialState, action)).toEqual(expectedState);
    });
});