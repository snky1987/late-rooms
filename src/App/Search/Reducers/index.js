import * as SearchActions from '../Actions';

const data = [
    {
        "name":       "hotelone",
        "starRating": 5,
        "facilities": ["car park", "pool"]
    },
    {
        "name":       "hoteltwo",
        "starRating": 3,
        "facilities": ["car park", "gym"]
    },
    {
        "name":       "hotelthree",
        "starRating": 3,
        "facilities": []
    }
];

export const initialState = {
    searchInput:         '',
    filters:             {
        starsRating: []
    },
    autocompleteOption:  '',
    autocompleteOptions: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SearchActions.INPUT_CHANGED:
            const value = action.value.toLowerCase();

            const autocompleteOptions = data.filter(option => {
                const optionLc = option.name.toLowerCase();

                return optionLc.indexOf(value) !== -1;
            });

            return {
                ...state,
                searchInput:         action.value,
                autocompleteOptions
            };
        case SearchActions.AUTOSELECT_OPTION_CLICKED:

            return {
                ...state,
                autocompleteOption: action.name
            };

        case SearchActions.FILTER_OPTION_CLICKED:
            let filterOptions = state.filters[action.name];
            let autocompleteOpts = JSON.parse(JSON.stringify(state.autocompleteOptions));

            if (filterOptions.indexOf(action.value) === -1) {
                filterOptions.push(action.value);
            }
            else {
                filterOptions = filterOptions.filter(val => val !== action.value);
            }

            return {
                ...state,
                autocompleteOptions: autocompleteOpts,
                filters: {
                    ...state.filters,
                    [action.name]: filterOptions
                }
            };
        default:
            return state;
    }
};