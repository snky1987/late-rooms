import React, { Component } from 'react';
import Input from './input';
import {connect} from "react-redux";
// Components
import Option from './autocompleteOption';


class Autocomplete extends Component {
    applyFilters() {
        const {SearchFormReducers} = this.props;
        const {
            filters,
            autocompleteOptions
        } = SearchFormReducers;

        let filteredOptions = autocompleteOptions;

        if (filters['starsRating'].length > 0) {
            filteredOptions = autocompleteOptions.filter(option => filters.starsRating.indexOf(option.starRating) !== -1);
        }

        return filteredOptions;
    }

    renderOptions() {
        const {SearchFormReducers} = this.props;
        const {
            searchInput
        } = SearchFormReducers;

        if (Boolean(searchInput) === false) {
            return null;
        }

        let filteredOptions = this.applyFilters();

        if (filteredOptions.length === 0) {
            const opt = {
                name: 'No Results Found'
            };

            return (
                <table className="table table-hover results">
                    <tbody>
                        <Option {...opt} key="no-results" noResultsOption={true} />
                    </tbody>
                </table>
            );
        }

        const options = filteredOptions.map((option, key) => {
            return (
                <Option {...option} key={key} />
            );
        });

        return (
            <table className="table table-hover results">
                <tbody>
                    {options}
                </tbody>
            </table>
        );
    }

    render() {
        const {
            label
        } = this.props;

        return (
            <div>
                <label>
                    {label}
                </label>
                <div>
                    <Input {...this.props} type="text" />
                </div>
                {this.renderOptions()}
            </div>
        );
    }
}


export default connect(
    state => state,
    () => {
        return {}
    }
)(Autocomplete);
