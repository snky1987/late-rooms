import React, { Component } from 'react';
import {connect} from "react-redux";
import * as SearchActions from "./Actions";

class AutocompleteOption extends Component {

    onOptionClick() {
        const {
            name,
            noResultsOption,
            onOptionClick
        } = this.props;

        if (noResultsOption === true) {
            return;
        }

        onOptionClick(name);
    }

    renderStarRating() {
        const {
            noResultsOption,
            starRating
        } = this.props;

        if (noResultsOption === true) {
            return;
        }

        let rating = '';

        for (let i = 1; i <= Number(starRating); i++) {
            rating += '*';
        }

        return `(${rating})`;
    }

    render() {
        const {
            props,
            onOptionClick
        } = this;

        const {
            name
        } = props;

        return (
            <tr className="table table-sm table-hover" onClick={onOptionClick.bind(this)}>
                <td>
                    <span className="results-title">
                        {name} {this.renderStarRating()}
                    </span>
                </td>
            </tr>
        );
    }
}

export default connect(
    state => state,
    dispatch => {
        return {
            onOptionClick: name => dispatch(SearchActions.autoselectOptionClicked(name)),
        }
    }
)(AutocompleteOption);
