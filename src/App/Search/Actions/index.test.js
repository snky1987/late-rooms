import * as actions from './index'
import {FILTER_OPTION_CLICKED} from "./index";

describe('App/Search/index', () => {
    it('should create an action to change input value and pass the value to the action', () => {
        const expectedAction = {
            type:  actions.INPUT_CHANGED,
            value: 'test'
        };

        expect(actions.inputChanged('test')).toEqual(expectedAction);
    });

    it('should create an action to change autoselect value and pass the name to the action', () => {
        const expectedAction = {
            type: actions.AUTOSELECT_OPTION_CLICKED,
            name: 'test'
        };

        expect(actions.autoselectOptionClicked('test')).toEqual(expectedAction);
    });

    it('should create an action to change filter value and pass the name and value to the action', () => {
        const expectedAction = {
            type:  actions.FILTER_OPTION_CLICKED,
            name:  'test',
            value: 1
        };

        expect(
            actions.filterChanged(
                {
                    name:  'test',
                    value: 1
                }
            )
        ).toEqual(expectedAction);
    });
});