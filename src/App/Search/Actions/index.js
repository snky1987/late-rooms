export const INPUT_CHANGED = 'SEARCH:ACTIONS:INPUT_CHANGED';
export const AUTOSELECT_OPTION_CLICKED = 'SEARCH:ACTIONS:AUTOSELECT_OPTION_CLICKED';
export const FILTER_OPTION_CLICKED = 'SEARCH:ACTIONS:FILTER_OPTION_CLICKED';

export const inputChanged = (value) => {
    return {
        type: INPUT_CHANGED,
        value
    }
};

export const autoselectOptionClicked = (name) => {
    return {
        type: AUTOSELECT_OPTION_CLICKED,
        name: name
    }
};

export const filterChanged = (options) => {
    const {
        name,
        value
    } = options;

    return {
        type: FILTER_OPTION_CLICKED,
        name: name,
        value
    }
};