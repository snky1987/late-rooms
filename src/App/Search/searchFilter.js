import React, { Component } from 'react';
import {connect} from "react-redux";
// Components
import SearchFilterOption from './searchFilterOption';

class SearchFilter extends Component {
    renderOptions() {
        const {
            name,
            options
        } = this.props;

        return options.map((option, key) => <SearchFilterOption key={`search-filter-${key}`} name={name} {...option} />);
    }

    render() {
        const {
            label
        } = this.props;

        return (
            <div>
                <span>
                    {label}
                </span>
                {this.renderOptions()}
            </div>
        );
    }
}


export default connect(
    state => state,
    () => {
        return {}
    }
)(SearchFilter);
