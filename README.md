# Late Rooms

Late Rooms Technical Test

## Dev notes to the test
I decided to split the application between auto-select and filters components.
Both components are using Redux to keep them connected with each-other in uni-directional data flow.

The test description suggests not to rush to finish this is why I focused only on stars rating filter.

There are two types of filtering applied:

1. Direct filter that belongs to Autoselect component and is driven by the input. This filter is applied by the reducer because both input and the result-set belogs to the same component.

2. External filter applied by another component. In order to keep separation of concerns this filter is applied on render and not by reducer.


The UI is ugly unfortunately. I decided to focus on delivering the engine in expense of the UI.

## Installation
1. In terminal please run command: 'npm install' to install all dependencies
2. In new terminal tab please run command: 'npm start'
3. Browser should automatically start with dev environment

## Testing
To execute unit tests please run command: 'npm test'.

I decided to test only the Actions and Reducers as they are core of this application.
I also do not have much experience in using JEST that allows to comprehensively test the UI.